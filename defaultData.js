export const defaultData = {
	avatar:'https://www.moyuu.cn/static/title.png',
	nickname:'moyuu',
	logoDefault:'',
	logoLarge:'',
	logo:'',
	appName:'Moyuu',
	appChineseName:'墨鱼',
	returnUrl:'pages/chat/msgList/msgList',
	version:'0.0.1',
};
